docker stop $(docker ps -a -q)
echo "DOCKER STOP ALL CONTAINERS"
docker rm $(docker ps -a -q)
echo "DOCKER REMOVE ALL CONTAINERS"
echo "INCEPEM CREAREA DE SSL"
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx.key -out nginx.crt
docker volume create portainer_data
echo "DOCKER VOLUME CREATE FOR PORTAINER"
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
echo "DOCKER BUILD PORTAINER - DONE"
docker build -t test:latest .
echo "DOCKER BUILD NGINX - OKAY"
docker run -p 8123:80 -p 8124:443 --name test -tid test
echo "----------------------DOCKER OKAY -------------"
echo "DOCKER RUN NGINX- OKAY PORT: 8124:443 / 8123:80"
echo "-----------------------------------------------"
